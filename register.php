<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>

    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" href="register.css">
</head>

<body>
    <?php
    session_start();
    $gender = array(
        '0' => 'Nam',
        '1' => 'Nữ'
    );

    $faculty = array(
        'MAT' => 'Khoa học máy tính',
        'KDL' => 'Khoa học vật liệu'
    );

    $allowedTypes = [
        'image/png' => 'png',
        'image/jpeg' => 'jpeg',
        'image/jpg' => 'jpg'
    ];
    $date = date("YmdHis");

    $nameErr = $genderErr = $khoaErr = $birthDayErr = $fileErr= "";
    $numErr = 0;
    $upload = 0; 

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $nameErr = "Hãy nhập tên";
            $numErr++;
        }

        if (empty($_POST["gender"])) {
            $genderErr = "Hãy chọn giới tính";
            $numErr++;
        }

        if (empty($_POST["khoa"])) {
            $khoaErr = "Hãy chọn phân khoa";
            $numErr++;
        }

        if (empty($_POST["date"])) {
            $birthDayErr = "Hãy nhập ngày sinh";
            $numErr++;

        } else if (!validateDate($_POST["date"])) {
            $birthDayErr = "Hãy nhập ngày sinh đúng định dạng";
            $numErr++;
        }

        $filepath = $_FILES['file']['tmp_name'];
        $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
        $filetype = finfo_file($fileinfo, $filepath);
        if (!in_array($filetype, array_keys($allowedTypes))) {
            $fileErr = "Please choose again. File is not image";
            $numErr++;
        }

        if ($numErr == 0) {
            $target_dir = "./upload/";
            $target_file = "";
            mkdir($target_dir, 0700);
            $nameImg = explode(".", $_FILES["file"]["name"]);
            $name = $nameImg[0] . "_" . $date . "." . $nameImg[1];
            $target_file = $target_dir . basename($name);
            move_uploaded_file($filepath, $target_file);
            $_POST["file"] = $target_file;
            $_POST["sss"] = $_FILES;
            $_SESSION = $_POST;
            header("Location: confirm.php");
        }
    }
    function input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    function validateDate($date)
    {
        $dates  = explode('/', $date);
        if (count($dates) == 3) {
            return checkdate($dates[1], $dates[0], $dates[2]);
        }
        return false;
    }
    ?>



    <form method="post" enctype="multipart/form-data" action="<?php
           echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <p class="error"><?php
                if ($nameErr != "") echo $nameErr . "<br>";
                if ($genderErr != "") echo $genderErr . "<br>";
                if ($khoaErr != "") echo $khoaErr . "<br>";
                if ($birthDayErr != "") echo $birthDayErr . "<br>";
                if ($fileErr != "") echo $fileErr . "<br>";

    ?></p>
        <div class="elements">
            <label class="label-input">Họ và tên <span class="error">* </span></label>
            <input class="input2" name="name" type="text" />
        </div>
        </div>
        <div class="elements">
            <label class="label-input">Giới tính <span class="error">* </span></label>
            <?php
            for ($i = 0; $i < count($gender); $i++) {
                echo "<input type=\"radio\" class=\"ip radio\" name=\"gender\" value=\"$gender[$i]\">
                    <label for=\"{$i}\" class=\"gender\">{$gender[$i]}</label>\n";
            };
            ?>
        </div>
        <div class="elements">
            <label class="label-input">Phân Khoa <span class="error">* </span></label>
            <select name="khoa" id="khoa" class="input khoa">
                <option value=""></option>
                <?php
                foreach ($faculty as $key => $value) {
                    echo "\t<option value=\"{$value}\">{$value}</option>\n";
                };
                ?>
            </select>
        </div>
        <div class="elements">
            <label class="label-input" for="date">Ngày sinh <span class="error">* </span></label>
            <input id="date" type="text" class="input date" name="date" placeholder="dd/mm/yyyy" />
        </div>
        <div class="elements">
            <label class="label-input">Địa chỉ</label>
            <input class="input2" type="text" name="address" />
        </div>
        <div class="elements">
            <label class="label-input">Hình ảnh</label>
            <input class="upload" type="file" name="file" accept=".jpg, .png" />
        </div>
        <button class="register submit">Đăng kí</button>
    </form>

    <script type="text/javascript">
        $('#date').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'dd/mm/yyyy',
        });
    </script>
</body>

</html>